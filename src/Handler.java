import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Handler implements Runnable{

    private Socket client;
    private BlockingQueue<String> queue;
    private BufferedReader buf;

    public Handler(Socket c, BlockingQueue<String> q){
        this.queue = q;
        this.client = c;

        try {
            buf = new BufferedReader(new InputStreamReader(client.getInputStream()));
        }catch(IOException ex){ex.printStackTrace();}

    }

    public void run(){
            boolean message_found = false;
            try {
                while (!message_found) {
                    if (buf.readLine().contains("<measure".toUpperCase())) {
                        message_found = true;
                        writeQueue();
                    } else {
                        buf.readLine();
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
    }

    public void writeQueue() {
        synchronized (queue) {
            try {
                for (int i = 0; i < 14; i++) {
                    try {
                        String c = buf.readLine();
                        queue.put(c);
                        System.out.println(c);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}