import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Parser implements Runnable{

    public BlockingQueue<String> queue;
    private boolean running = true;

    public Parser(BlockingQueue<String> q){
        this.queue = q;
    }

    public void run(){
        while(running) {
            if (queue.remainingCapacity() == 0) {
                writeToFile();
            }
        }
    }
    public void writeToFile(){
            System.out.println("Check2");
            String msg = "";
            msg = queue.peek();
            String station = "";
            if (msg.contains("STN")) {
                station = msg.substring(7, msg.length() - 6);
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date();
                String path = System.getProperty("user.dir") + "\\station_data\\" + station + "\\" + dateFormat.format(date);

                String station_2 = msg + "\r\n";

                //Set directory and file
                Path dir = Paths.get(path);
                Path file = dir.resolve("data.txt");

                ArrayList<String> list = new ArrayList<>(14);

                queue.drainTo(list);

                try {
                    //Check if the directory exists, if not create it
                    if (!Files.exists(dir)) {
                        Files.createDirectories(dir);
                    }
                    //Check if the file exists, if not create it
                    if (!Files.exists(file)) {
                        Files.write(file, list, Charset.forName("UTF-8"), StandardOpenOption.CREATE);
                    } else {
                        System.out.println("check?");
                        Files.write(file, list, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }else{
                System.out.println("Check3");
            }
    }
}